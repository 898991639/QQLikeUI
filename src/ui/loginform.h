#ifndef LOGINFORM_H
#define LOGINFORM_H

#include <QWidget>

namespace Ui {
class LoginForm;
}

class LoginForm : public QWidget
{
    Q_OBJECT

public:
    explicit LoginForm(QWidget *parent = 0);
    ~LoginForm();

protected:
      void mousePressEvent(QMouseEvent *event);
private:
    Ui::LoginForm *ui;
};

#endif // LOGINFORM_H
